public class Cleric extends Character {

    public Cleric(String name) {
        this.name = name;
        this.level = 1;
        this.baseHitpoints = 700;
        this.baseManapoints = 800;
        this.baseAgility = 5;
        this.baseStrength = 10;
        this.baseIntelligence = 15;
    }

    public String dropLeftWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setLeftWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.leftHandWeapon == null){
            if(weapon instanceof Mace && (this.rightHandWeapon == null || !(this.rightHandWeapon instanceof Mace))){
                this.leftHandWeapon = weapon;
            }else if(weapon instanceof Shield && (this.rightHandWeapon == null || !(this.rightHandWeapon instanceof Shield))){
                this.leftHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getClass().getSimpleName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

    public String dropRightWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setRightWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.rightHandWeapon == null){
            if(weapon instanceof Mace && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof Mace))){
                this.rightHandWeapon = weapon;
            }
            else if(weapon instanceof Shield && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof Shield))){
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

}
